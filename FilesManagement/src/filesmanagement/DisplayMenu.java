/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filesmanagement;

import java.util.Scanner;

/**
 *
 * @author lionel Mangoua
 */
public class DisplayMenu {

    public static void Menu() {

        try {
            Scanner input = new Scanner(System.in);

            System.out.println("=============================================================");
            System.out.println("===     Welcome To File Management System                 ===");
            System.out.println("===     Follow the instuctions below:                     ===");
            System.out.println("===                                                       ===");
            System.out.println("===     Enter the following options to work on the system ===");
            System.out.println("===        1 - To create a file                           ===");
            System.out.println("===        2 - To read and write a file                   ===");
            System.out.println("===        3 - To generate a log file                     ===");
            System.out.println("===        4 - To list all files on console               ===");
            System.out.println("===        5 - To exit the system                         ===");
            System.out.println("=============================================================");
            System.out.println();
            System.out.println("Please enter your choice here ==> ");

            int userOption = input.nextInt();

            do {
                switch (userOption) {
                    case 1:
                        CreateFile.writeOnBinFiles();
                        break;
                    case 2:
                        CreateFile.writeOnBinFiles();
                        break;
                    case 3:
                        LogFile.writeLogFile();
                        break;
                    case 4:
                        LogFile.listFilesFromFolder();
                        break;
                    default:
                        System.out.println("Error. Invalid Entry....");
                        System.out.println();
                }
                    System.out.println("Please enter your choice here ==> ");
                    userOption = input.nextInt();
                
                    if (userOption == 5) {
                        System.out.println("--------Good Bye--------------");
                    } 
                
            }while (userOption != 5);
        } 
        catch (Exception e) {
            System.out.println("Error!!! INPUT NOT ALLOWED....");
        }
    }

}
